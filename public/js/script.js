var movies=[]
 var favourites=[];
function getMovies() {

	var xhr= new XMLHttpRequest();

	xhr.open("get", "http://localhost:3000/movies");
	xhr.send(null)

	xhr.onload = function() {
		movies=JSON.parse(xhr.responseText);
		//console.log(movies);
		for(let i=0; i<movies.length;i++) {
			document.getElementById("moviesList").innerHTML += 
			`<div class="product">
			<li><h6>${movies[i].title}</h6></li>
			<img src=${movies[i].posterPath} alt="${movies[i].title}"></img><br><br>
			<button type="button" class="btn btn-primary" onclick="addFavourite(${movies[i].id})">Add to Fav </button> 
			</div><br>`
		}
	}
}

function getFavourites() {
   var movies=[];
	var xhr=new XMLHttpRequest();
	console.log(xhr.readyState);
    xhr.open("get","http://localhost:3000/favourites");
	xhr.send(null)

	xhr.onload=function() {
		favourites= JSON.parse(xhr.responseText);
		console.log(favourites);
		for(let i=0;i<favourites.length;i++){
			document.getElementById("favouritesList").innerHTML +=
			`<div class="product">
			<li><h6>${favourites[i].title}</h6></li>
			<img src=${favourites[i].posterPath} alt="${favourites[i].title}"><br><br>
			<button type="button" class="btn btn-primary" onclick="delFavourite(${favourites[i].id})">Delete </button> 
			</div><br>`
		}
	}
}

function addFavourite(id) {
	//var fav=[]
	console.log("Added to favourite");
	let obj=getMovieInfoById(id);
    //fav.push(obj)
	//console.log(fav)

	return fetch("http://localhost:3000/favourites",
	{
		method: 'POST', 
        body: JSON.stringify(obj), 
        headers: { "Content-type": "application/json" 
    } 
}) 
 then(res => res.json()) 
.then(json => console.log(id))
}	

const getMovieInfoById= (movieId) => {
	for(let i=0;i<movies.length;i++) {
		if(movieId == movies[i].id) {
			console.log(movies[i]);
			return movies[i];
		}
	}
}

const delFavourite =(favId) => {
	console.log("Movie id = ${favId} deleted");
    return fetch(`http://localhost:3000/favourites/${favId}`,
	{
		method: 'DELETE', 
        headers: { "Content-type": "application/json" }
	}
     ) 
}
   
// module.exports = {
// 	getMovies,
// 	getFavourites,
// 	addFavourite,
// 	delFavourite
// };